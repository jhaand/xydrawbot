# XYdrawBot

An XY axis Drawing Robot that is build according to the design of Plexi on Printables.com. 

This repository will host all the necessary files and build instructions. 

## Printing

Printed on a Prusa Mini+. Use 75 % infill and PLA. 
I used Azure Blue Prusament and generic White from 123-3D.nl . 

Parts to print 
| File | Printed |
| ----- | ------ | 
| STL_originals/base_slide_heavy_duty_ha.stl | |
| STL_originals/base_slide_heavy_duty_v4.stl | |
| STL_originals/bottom_xy_clamshell_v2.stl | D |
| STL_originals/cablestrainreliefanchorv2.stl | |
| STL_originals/drawbot_uno_enclosure_mount_base_v1.stl | |
| STL_originals/drawbot_uno_enclosure_mount_upright_v1.stl | |
| STL_originals/large_display_pen_holder_v2_137296.stl | |
| STL_originals/metric_3_mm_thumb_screw_v4.stl | |
| STL_originals/pen_holder.stl | |
| STL_originals/pen_holder_v2.stl | |
| STL_originals/slider_heavy_duty_v5.stl | |
| STL_originals/slider.stl | |
| STL_originals/top_xy_clamshell_v4.stl | D |
| STL_originals/uno_cnc_back.stl | |
| STL_originals/uno_cnc_bottom.stl | |
| STL_originals/uno_cnc_front.stl | |
| STL_originals/uno_cnc_top.stl | |
| STL_originals/x_support_l.stl | D |
| STL_originals/x_support_r.stl | D |
| STL_originals/y_back.stl | D |
| STL_originals/y_front.stl | D |

## Building

See the building manual in `./doc/` 

## Firmware

The XYdrawBot uses a custom build of GRBL with servo support. 

When the code compiles successful. You need to support CoreXY support in the config.h of the grbl-servo library. 
Uncomment in  `~/Arduino/libraries/grbl-servo-master$ ` the following line in  `config.h`: \
```#define COREXY // Default disabled. Uncomment to enable.``

Otherwise the XYdrawBot will draw lines in 45 degrees.

## Drawing 

Use the Universal G-Code sender to  the Arduino 
https://github.com/winder/Universal-G-Code-Sender

## Inspiration. 

This robot is based on the "Drawing Robot - Arduino Uno + CNC Shield + GRBL" by Plexi on Printables.com You can find the design here: 
https://www.printables.com/model/137296-drawing-robot-arduino-uno-cnc-shield-grbl/files

Add the pully extensions from this build

https://www.thingiverse.com/thing:2424284/files
